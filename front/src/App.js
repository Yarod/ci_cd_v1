import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Mangekyo_sharingan_6ke.svg/600px-Mangekyo_sharingan_6ke.svg.png?20140305025656"
          className="App-logo"
          alt="logo"
        />
        <p>Eternal Sharingan</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React by Doray
        </a>
      </header>
    </div>
  );
}

export default App;
